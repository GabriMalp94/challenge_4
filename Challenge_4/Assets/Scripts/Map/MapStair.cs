﻿
using UnityEngine;

public class MapStair : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject == GameManager.Instance.LocalPlayer.gameObject)
        {
            GameManager.Instance.NextLevel();
        }
    }
}
