﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryHud : MonoBehaviour
{
    [SerializeField] private InventorySprite inventorySpritePrefab;
    private List<InventorySprite> spriteInstance = new List<InventorySprite>();

    private void OnEnable()
    {
        if(Inventory.Instance)
        Inventory.Instance.inventoryUpdate += InventoyRefresh;
    }

    private void OnDisable()
    {
        if (Inventory.Instance)
            Inventory.Instance.inventoryUpdate -= InventoyRefresh;
    }
    public void InventoyRefresh()
    {
        foreach(var sprite in spriteInstance)
        {
            Destroy(sprite.gameObject);
        }
        spriteInstance.Clear();

        foreach(var item in Inventory.Instance.ItemInventory)
        {
            InventorySprite currentItem = Instantiate(inventorySpritePrefab, this.transform);
            currentItem.SetInventoryItem(item.Key, item.Value);
            spriteInstance.Add(currentItem);
        }

    }
    
}
