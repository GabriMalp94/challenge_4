﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ItemLootPotrait : MonoBehaviour
{
    [SerializeField] private Image image;

    [SerializeField] private TextMeshProUGUI description;
    [SerializeField] float duration;

    private void Start()
    {
        Destroy(this.gameObject, duration);
    }

    public void SetPotrait(Item _item)
    {
        image.sprite = _item.ItemSprite;
        description.text = _item.ItemName;
    
        switch(_item)
        {
            case Armor armor:
                image.color = armor.SpriteColor;
                break;
            case Weapon weapon:
                image.color = weapon.ItemColor;
                break;
        }
    }
}
