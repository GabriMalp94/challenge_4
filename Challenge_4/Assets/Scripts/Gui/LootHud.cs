﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootHud : MonoBehaviour
{
    [SerializeField] private ItemLootPotrait potraitPrefab;
    [SerializeField] private float potraitDelay = 0.2f;

    private void OnEnable()
    {
        Debug.Log("enabe");
        if(Inventory.Instance != null)
        {
            Inventory.Instance.onChestOpenEvent += OnLoot;
            Debug.Log("setting event");
        }
        else
        {
            Debug.Log("instance is null");
        }
    }

    private void OnDisable()
    {
        if (Inventory.Instance)
        {
            Inventory.Instance.onChestOpenEvent -= OnLoot;

        }
        Debug.Log("disable");
    }

    public void OnLoot(List<Item> itemList)
    {
        StartCoroutine(SetPotraits(itemList));
    }

    private IEnumerator SetPotraits (List<Item> _itemList)
    {
        foreach(var item in _itemList)
        {
            ItemLootPotrait currentPotrait = Instantiate(potraitPrefab, this.transform);
            currentPotrait.SetPotrait(item);
            yield return new WaitForSeconds(potraitDelay);
        }
        yield return null;
    }
}
