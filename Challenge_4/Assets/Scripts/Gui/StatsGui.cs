﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StatsGui : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI health;
    [SerializeField] private TextMeshProUGUI mana;
    [SerializeField] private TextMeshProUGUI power;
    [SerializeField] private TextMeshProUGUI defence;

    [SerializeField] private Image weapon;
    [SerializeField] private Image armor;


    public void SetHealth()
    {
        health.text = "Health: " + GameManager.Instance.LocalPlayer.CurrentStamina + "/" + GameManager.Instance.LocalPlayer.Stamina;
    }

    public void SetMana()
    {
        mana.text = "energy: " + Mathf.RoundToInt(GameManager.Instance.LocalPlayer.CurrentMana) + "/" + GameManager.Instance.LocalPlayer.BaseMana;
    }

    public void SetPower()
    {
        power.text = "power: " + GameManager.Instance.LocalPlayer.BasePower;
    }

    public void SetDefence()
    {
        defence.text = "defence: " + GameManager.Instance.LocalPlayer.BaseArmor;
    }

    public void SetWeaponSprite()
    {
        weapon.sprite = GameManager.Instance.LocalPlayer.WeaponEQuip.ItemSprite;
        weapon.color = GameManager.Instance.LocalPlayer.WeaponEQuip.ItemColor;

    }

    public void SetArmorSprite()
    {
        armor.sprite = GameManager.Instance.LocalPlayer.ArmorEquip.ItemSprite;
        armor.color = GameManager.Instance.LocalPlayer.ArmorEquip.SpriteColor;
            
    }
}
