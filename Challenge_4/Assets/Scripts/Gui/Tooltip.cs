﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Tooltip : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI title;
    [SerializeField] private TextMeshProUGUI description;

    public void setTooltip(string _title, string _description)
    {
        title.text = _title;
        description.text = _description;
    }
}
