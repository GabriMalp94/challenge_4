﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using static UnityEngine.EventSystems.PointerEventData;

public class InventorySprite : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    private Item currentItem;

    private Image image;
    private TextMeshProUGUI textMesh;

    [SerializeField] private Tooltip tooltipPrefab;
    private Tooltip currentTooltip;



    public void OnPointerEnter(PointerEventData eventData)
    {
        currentTooltip = Instantiate(tooltipPrefab, transform);
        currentTooltip.setTooltip(currentItem.ItemName, currentItem.ItemDescription);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Destroy(currentTooltip.gameObject);
    }

    public void SetInventoryItem(Item _item, int _ammount)   
    {
        image = GetComponentInChildren<Image>();
        textMesh = GetComponentInChildren<TextMeshProUGUI>();
        currentItem = _item;
        image.sprite = currentItem.ItemSprite;
        textMesh.text = _ammount > 1 ? _ammount.ToString() : "";


        switch (_item)
        {
            case Armor armor:
                image.color = armor.SpriteColor;
                break;

            case Weapon weapon:
                image.color = weapon.ItemColor;
                break;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if(eventData.button.Equals(InputButton.Right))
        {
            currentItem.ItemEffect();
        }
    }
}
