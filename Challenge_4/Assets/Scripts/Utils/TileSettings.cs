﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TilesType
{
    floor,
    sxBorder,
    dxBorder,
    upBorder,
    downBorder,
    sxDownAngle,
    dxDownAngle,
}

[System.Serializable]
public class TileSettings
{
    [SerializeField] private string GroupName;

    [SerializeField] private TilesType mapTileType;
    public TilesType MapTileType { get { return mapTileType; } }

    [SerializeField] private SpriteRenderer tilePrefab;
    public SpriteRenderer TilePrefab { get { return tilePrefab;} }

    [SerializeField] private List<Sprite> spritesList;
    public List<Sprite>SpritesList { get { return spritesList; } }
}
