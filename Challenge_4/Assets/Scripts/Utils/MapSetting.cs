﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class MapSetting
{
    [SerializeField] private Stats playerPrefab;
    public Stats PlayerPrefab { get { return playerPrefab; } }

    [SerializeField] private int mapLenght;
    public int MapLenght { get { return mapLenght; } }

    [SerializeField] private int mapHeight;
    public int MapHeight { get { return mapHeight; } }

    [SerializeField] private List<TileSettings> tilesList;
    public List<TileSettings> TilesList { get { return tilesList; } }

    [SerializeField] private List<SpawnableObject> spawnableObjectList;
    public  List<SpawnableObject> SpawnableObjectList { get { return spawnableObjectList; } }

    [SerializeField] private Stats enemy;
    public Stats Enemy { get { return enemy; } }

    [SerializeField] private GameObject nextLevelStair;
    public GameObject NextLevelStair { get { return nextLevelStair; } }

    [SerializeField] private GameObject background;
    public GameObject Background { get { return background; } }
}
