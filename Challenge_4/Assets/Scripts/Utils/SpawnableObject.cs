﻿using UnityEngine;

[System.Serializable]
public class SpawnableObject
{
    [SerializeField] private GameObject objectPrefab;
    public GameObject ObjectPrefab { get { return objectPrefab; } }

    [SerializeField] private int spawnRate;
    public int SpawnRate { get { return spawnRate; } }
}
