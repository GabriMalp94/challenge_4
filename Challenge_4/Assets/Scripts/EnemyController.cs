﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private float speed = 2;
    [SerializeField] List<Sprite> directionSprites;
    private SpriteRenderer spriteRenderer;

    [SerializeField] private float damage;

    private Stats player;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        Move();
    }

    private void Move()
    {
        Vector3 direction = GameManager.Instance.LocalPlayer.transform.position - transform.position;
        SpriteSelection(direction.normalized);
        transform.position = Vector3.MoveTowards(transform.position, GameManager.Instance.LocalPlayer.transform.position, speed * Time.deltaTime);
    }

    private void SpriteSelection(Vector3 direction)
    {
        if(direction.y < -0.5)
        {
            SetSprite(directionSprites[0]);
        }
        else if (direction.y > 0.5)
        {
            SetSprite(directionSprites[1]);
        }
        else if (direction.x < -0.5)
            {
                SetSprite(directionSprites[2]);
            }
        else if (direction.x > 0.5)
            {
                SetSprite(directionSprites[3]);
            }
    }

    private void SetSprite(Sprite _sprite)
    {
        spriteRenderer.sprite = _sprite;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent<Stats>(out Stats currentPlayer) && currentPlayer.CurrentEntityType == EntityType.player)
        {
            player = currentPlayer;
            player.HealthVariateion(-damage - (damage * GameManager.Instance.CurrentLevel * GameManager.Instance.DifficultModifier));
            StartCoroutine(PlayerDamageCoroutine());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.TryGetComponent<Stats>(out Stats currentPlayer) && currentPlayer.CurrentEntityType == EntityType.player)
        {
            player = null;
            StopAllCoroutines();
        }
    }

    IEnumerator PlayerDamageCoroutine()
    {
        while(player)
        {
            yield return new WaitForSeconds(1);
            player.HealthVariateion(-damage -(damage * GameManager.Instance.CurrentLevel * GameManager.Instance.DifficultModifier));
            yield return null;
        }
            yield return null;
    }
}
