﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum EntityType
{
    player,
    enemy,
}

public class Stats : MonoBehaviour
{
    public Action<Equipable> armorChangeEvent;
    public Action<Item> onEquipChange;

    [SerializeField] private float stamina = 100;
    public float Stamina { get { return stamina; } }

    private float currentStamina = 0;
    public float CurrentStamina
    {
        get
        {
            return currentStamina;
        }
    }

    [SerializeField] private float baseMana = 100;
    public float BaseMana { get { return baseMana; } }

    private float currentMana = 0;
    public float CurrentMana
    {
        get
        {
            return currentMana;
        }

        set
        {
            currentMana = value;
            if(currentMana <0)
            {
                currentMana = 0;
            }
            else if(currentMana > baseMana)
            {
                currentMana = baseMana;
            }

            HudManager.Instance.statsGui.SetMana();
        }
    }

    [SerializeField] private float baseArmor;
    public float BaseArmor
    {
        get
        {
            if (ArmorEquip)
                return baseArmor + ArmorEquip.ArmorValue;
            else
                return baseArmor;
        }
    }

    [SerializeField] private float basePower;
    public float BasePower
    {
        get
        {
            if (weaponEquip)
                return basePower + weaponEquip.Damage;
            else
                return basePower;
        }
    }

    [SerializeField] private EntityType currentEntityType;
    public EntityType CurrentEntityType { get { return currentEntityType; } }

    
    [SerializeField] private Armor armorEquip;
    public Armor ArmorEquip { get { return armorEquip; } }

    Equipable currentArmorInstance;

    [SerializeField] private Weapon weaponEquip;
    public Weapon WeaponEQuip { get { return weaponEquip; } }
 
    private void Awake()
    {
        currentMana = baseMana;
        currentStamina = stamina;
    }

    private void Start()
    {
        if(ArmorEquip)
        {
            ArmorChange(ArmorEquip);
        }
        HudManager.Instance.statsGui.SetPower();
        HudManager.Instance.statsGui.SetWeaponSprite();
    }

    public void shoot(Vector3 direction)
    {
        if(currentMana > 0)
        {
            Debug.Log("spara la croce");

            Shootable crucifix = Instantiate(weaponEquip.WeaponPrefab, GameManager.Instance.MapContainer.transform);
            crucifix.transform.position = transform.position;

            Debug.DrawRay(transform.position, direction);

            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);

            crucifix.transform.rotation = Quaternion.AngleAxis(angle-90, Vector3.forward);
            crucifix.SetShootWeapon(weaponEquip.Damage, weaponEquip.ItemSprite, weaponEquip.ItemColor);

            CurrentMana -= 1;
        }
    }

    public void ArmorChange(Armor _armor)
    {
        if(currentArmorInstance)
        {
            Destroy(currentArmorInstance.gameObject);
        }

        armorEquip = _armor;
        currentArmorInstance = Instantiate(armorEquip.ArmorPrefab, this.transform);
        currentArmorInstance.ArmorSpriteRenderer.color = armorEquip.SpriteColor;

        if(armorChangeEvent != null)
        {
            armorChangeEvent.Invoke(currentArmorInstance);
        }
        HudManager.Instance.statsGui.SetArmorSprite();
        HudManager.Instance.statsGui.SetDefence();
    }

    public void ItemEquip(Item _item) 
    {
        if(Inventory.Instance.ItemInventory.ContainsKey(_item))
        {
            Inventory.Instance.ConsumeItem(_item);
            switch(_item)
            {
                case Armor a:
                    Inventory.Instance.ItemAdd(armorEquip);
                    ArmorChange(a);
                    break;

                case Weapon w:
                    Inventory.Instance.ItemAdd(weaponEquip);
                    weaponEquip = w;
                    HudManager.Instance.statsGui.SetPower();
                    HudManager.Instance.statsGui.SetWeaponSprite();
                    break;

                default:
                    Debug.LogError("trying to equip not suitable item");
                    break;
            }

        }
    }
    
    public void HealthVariateion(float ammount)
    {
        if(ammount < 0)
        {
            float damage = ammount + BaseArmor;
            if(damage > 0)
            {
                damage = 0;
            }
            ammount = damage;
        }

        currentStamina += ammount;
        currentStamina = Mathf.RoundToInt(currentStamina);
        if(currentStamina <= 0)
        {
            GameManager.Instance.OnEntityDie(CurrentEntityType);
            Destroy(this.gameObject);
        }

        if(currentStamina > stamina)
        {
            currentStamina = stamina;
        }

        HudManager.Instance.statsGui.SetHealth();
    }
}
