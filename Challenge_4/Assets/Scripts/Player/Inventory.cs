﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Inventory : MonoBehaviour
{
    private static Inventory instance;
    public static Inventory Instance { get { return instance; } }

    public event Action inventoryUpdate;
    public event Action<List<Item>> onChestOpenEvent;

    private Dictionary<Item, int> itemInventory = new Dictionary<Item, int>();
    public Dictionary<Item, int> ItemInventory { get { return itemInventory; } }

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Debug.Log("setting instance inventory");
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void OnLoot(List<Item> _itemList)
    {
        if (onChestOpenEvent != null)
            onChestOpenEvent.Invoke(_itemList);

        foreach(var lootedItem in _itemList)
        {
            bool isStored = false;

            foreach(var storedItem in itemInventory)
            {
                if(storedItem.Key.ItemName == lootedItem.ItemName)
                {
                    itemInventory[storedItem.Key]++;
                    isStored = true;
                    break;
                }
            }
            if(!isStored)
            {
                itemInventory.Add(lootedItem, 1);
            }
        }

        if(inventoryUpdate != null)
            inventoryUpdate.Invoke();
    }

    public void ConsumeItem(Item _item)
    {

        foreach (var item in itemInventory)
        {
            if (item.Key.ItemName == _item.ItemName)
            {
                itemInventory[item.Key]--;
                if(itemInventory[item.Key] <= 0)
                {
                    itemInventory.Remove(item.Key);
                }
                break;
            }
        }

        if (inventoryUpdate != null)
        {
            inventoryUpdate.Invoke();
        }
    }

    public void ItemAdd(Item _item)
    {
        bool addItemNewSpot = true;
        foreach(var item in itemInventory)
        {
            if(item.Key.ItemName == _item.ItemName)
            {
                itemInventory[item.Key]++;
                addItemNewSpot = false;
                break;
            }
        }
        if(addItemNewSpot)
        {
            itemInventory.Add(_item, 1);
        }
        if(inventoryUpdate != null)
        {
            inventoryUpdate.Invoke();
        }
    }

}
