﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class MovementInput : MonoBehaviour
{
    [SerializeField] private float speed;

    private EntityAnimator playerAnimation;

    private Equipable currentArmor;

    private Stats entityStats;
    private Rigidbody2D rigidbody;

    Vector3 direction;

    [SerializeField] private float manaConsumePerSecond = 1;

    bool isMoving;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        playerAnimation = GetComponent<EntityAnimator>();
        entityStats = GetComponent<Stats>();
        GetComponent<Stats>().armorChangeEvent += setEquip;
    }

    // Update is called once per frame
    void Update()
    {
        Rotate();
        if(entityStats.CurrentMana > 0)
        {
            Move();
        }
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            entityStats.shoot(direction);
        }
    }



    private void Rotate()
    {

        Vector3 pos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y , 10));

        pos.z = 0;
        direction = pos - transform.position;

        playerAnimation.SetAnimationMove(direction.normalized,isMoving);
        if (currentArmor)
        {
            currentArmor.EquipAnimator.SetAnimationMove(direction.normalized, isMoving);
        }
    }

    private void Move()
    {
        float deltaY = Input.GetAxisRaw("Vertical") * speed;
        float deltaX = Input.GetAxisRaw("Horizontal") * speed; ;

        Vector3 move = new Vector3(deltaX, deltaY, 0);

        if(move != Vector3.zero)
        {
            entityStats.CurrentMana -= manaConsumePerSecond * Time.deltaTime;
        }

        isMoving = move != Vector3.zero ? true : false;

        move = Vector3.ClampMagnitude(move, speed);
        move *= Time.deltaTime;

        rigidbody.MovePosition(transform.position + move);
    }


    private void setEquip(Equipable _armor)
    {
        currentArmor = _armor;
    }
}
