﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityAnimator : MonoBehaviour
{
    [SerializeField] Animator animator;

    private void Start()
    {
        setIdleDirection(0);
    }

    public void SetAnimationMove(Vector3 move, bool isMoving)
    {
        if(move.y > 0.5)
        {
            setIdleDirection(1);
        }
        else if (move.x > 0.5)
        {
            setIdleDirection(2);
        }
        else if (move.y < -0.5)
        {
            setIdleDirection(0);
        }
        else if (move.x < -0.5)
        {
            setIdleDirection(3);
        }

        animator.SetFloat("Vertical", move.y);
        animator.SetFloat("Horizontal", move.x);

        animator.SetBool("isMoving", isMoving);
       
    }

    private void setIdleDirection(int _value)
    {
        animator.SetFloat("IdlePosition", _value);
    }
}
