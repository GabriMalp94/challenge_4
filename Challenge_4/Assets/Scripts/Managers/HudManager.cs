﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HudManager : MonoBehaviour
{
    private static HudManager instance;
    public static HudManager Instance { get { return instance; } }

    [SerializeField] private LootHud lootHud;
    [SerializeField] private InventoryHud inventoryHud;
    [SerializeField] private StatsGui _statsGui;

    [SerializeField] private GameObject startPanel;
    [SerializeField] private GameObject gameplayPanel;
    [SerializeField] private GameObject gamoverPanel;

    public StatsGui statsGui { get { return _statsGui; } }
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void SetChangeStatusPanel(GameStatus _status)
    {
        switch(_status)
        {
            case GameStatus.start:
                startPanel.SetActive(true);
                gameplayPanel.SetActive(false);
                gamoverPanel.SetActive(false);
                break;

            case GameStatus.run:
                startPanel.SetActive(false);
                gameplayPanel.SetActive(true);
                gamoverPanel.SetActive(false);
                break;

            case GameStatus.gameover:
                startPanel.SetActive(false);
                gameplayPanel.SetActive(false);
                gamoverPanel.SetActive(true);
                break;
        }
    }
}
