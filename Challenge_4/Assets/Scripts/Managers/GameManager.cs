﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public enum GameStatus
{
    start,
    run,
    gameover,
}

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public static GameManager Instance { get { return instance; } }

    [SerializeField] private MapSetting mapSetting;

    private GameObject mapContainer;
    public GameObject MapContainer { get { return mapContainer; } }


    private Stats localPlayer;
    public Stats LocalPlayer { get { return localPlayer; } }

    [SerializeField] float difficultModifier = 0.2f;
    public float DifficultModifier { get { return difficultModifier; } }

    private float currentLevel = 0;
    public float CurrentLevel { get { return currentLevel; } }

    [SerializeField] private int EnemyCount;
    List<Stats> spawnedEnemy = new List<Stats>();

    private GameStatus _gameStatus;
    public GameStatus gameStatus { get { return _gameStatus; } }

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        SetGameStatus(GameStatus.start);
        
    }

    public void Restat()
    {
        SetGameStatus(GameStatus.run);
        localPlayer = Instantiate(mapSetting.PlayerPrefab);
        localPlayer.transform.position = new Vector3(mapSetting.MapLenght / 2, mapSetting.MapHeight / 2, 0);
        currentLevel = 0;
        MapInitialize();
    }

    public void NextLevel()
    {
        MapInitialize();
    }

    private void MapInitialize()
    {
        if(mapContainer)
        {
            Destroy(mapContainer);
        }

        spawnedEnemy.Clear();
        mapContainer = new GameObject();
        mapContainer.name = "MapContainer";

        Debug.Log(mapSetting.MapLenght + " x " + mapSetting.MapHeight);
        for(int i = 0; i < mapSetting.MapLenght; i++)
        {
            for(int j = 0; j < mapSetting.MapHeight; j++)
            {
                GameObject currentMapTile = null;
                Vector3 tailPosition = new Vector3(i, j, 0);


                if(!IsBorder(i, j, out currentMapTile))
                {
                    currentMapTile = GetMapTile(TilesType.floor);
                    SetSpawnableObject(tailPosition);

                }
                currentMapTile.transform.position = tailPosition;

            }
        }

        for(int i = 0; i <EnemyCount + EnemyCount * currentLevel * difficultModifier; i++)
        {
            Stats currentEnemy = Instantiate(mapSetting.Enemy, mapContainer.transform);
            float randomX = Random.Range(0, mapSetting.MapLenght);
            float randomY = Random.Range(0, mapSetting.MapHeight);
            currentEnemy.transform.position = new Vector3(randomX, randomY, 0);
            spawnedEnemy.Add(currentEnemy);
        }

        Camera.main.transform.position = new Vector3(mapSetting.MapLenght / 2, mapSetting.MapHeight / 2, Camera.main.transform.position.z);

        GameObject background = Instantiate(mapSetting.Background, mapContainer.transform);
        background.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, 0);
    }

    private void SetSpawnableObject(Vector3 _position)
    {
        int randomPerc = Random.Range(0, 100);

        foreach(var obj in mapSetting.SpawnableObjectList)
        {
           if(obj.SpawnRate > randomPerc)
           {
                GameObject currentObject = Instantiate(obj.ObjectPrefab, mapContainer.transform);
                currentObject.transform.position = _position;
           }
           else

            {
                randomPerc -= obj.SpawnRate;
            }
        }
    }

    private bool IsBorder(int i, int j, out GameObject tail)
    {
        if(i == 0)
        {
            if (j == 0)
            {
                tail = GetMapTile(TilesType.sxDownAngle);
            }
            else
            {
                tail = GetMapTile(TilesType.sxBorder);

            }
            return true;
        }
        else if( i == mapSetting.MapLenght - 1)
        {
            if (j == 0)
            {
                tail = GetMapTile(TilesType.dxDownAngle);
            }
            else
            {
                tail = GetMapTile(TilesType.dxBorder);
            }
            return true;
        }
        else if(j == 0)
        {
            tail = GetMapTile(TilesType.downBorder);
            return true;
        }

        else if(j == mapSetting.MapHeight-1)
        {
            tail = GetMapTile(TilesType.upBorder);
            return true;
        }
        tail = null;
        return false;
    }

    private GameObject GetMapTile(TilesType _type)
    {
        TileSettings currentTile = (from map in mapSetting.TilesList where map.MapTileType.Equals(_type) select map).First();


        int prefabIndex = Random.Range(0, currentTile.SpritesList.Count); ;
        SpriteRenderer currentMap = Instantiate(currentTile.TilePrefab, mapContainer.transform);

        currentMap.sprite = currentTile.SpritesList[prefabIndex];

        return currentMap.gameObject;
    }

    public void OnEntityDie(EntityType _type)
    {
        switch(_type)
        {
            case EntityType.enemy:
                NextLevelCheck();
                break;

            case EntityType.player:
                SetGameStatus(GameStatus.gameover);
                break;
        }
    }

    private void NextLevelCheck()
    {
        bool nextLevel = true;

        foreach(var e in spawnedEnemy)
        {
            if(e.CurrentStamina > 0)
            {
                nextLevel = false;
                break;
            }
        }

        if(nextLevel)
        {
            Debug.Log("prepare for next level");
            GameObject stair = Instantiate(mapSetting.NextLevelStair, mapContainer.transform);
            stair.transform.position = new Vector3(mapSetting.MapLenght / 2, mapSetting.MapHeight / 2, 0);
            currentLevel++;
        }
    }

    private void SetGameStatus(GameStatus _status)
    {
        _gameStatus = _status;
        HudManager.Instance.SetChangeStatusPanel(_status);
    }

   public void Quit()
    {
        Application.Quit();
    }
}
