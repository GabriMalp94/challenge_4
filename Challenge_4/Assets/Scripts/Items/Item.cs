﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName =("custom/items"))]
public class Item : ScriptableObject
{
    [SerializeField] private string itemName;
    public string ItemName { get { return itemName; } }

    [SerializeField] private string itemDescription;
    public string ItemDescription { get { return itemDescription; } }

    [SerializeField] private Sprite itemSprite;
    public Sprite ItemSprite { get { return itemSprite; } }

    [SerializeField] private float dropRate;
    public float DropRate { get { return dropRate; } }


    public virtual void ItemEffect()
    {

    }
}
