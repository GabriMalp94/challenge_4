﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("custom/items/weapon"))]
public class Weapon : Item
{
    [SerializeField] private float damage;
    public float Damage { get { return damage; } }

    [SerializeField] private Color itemColor;
    public Color ItemColor { get { return itemColor; } }

    [SerializeField] private Shootable weaponPrefab;
    public Shootable WeaponPrefab { get { return weaponPrefab; } }

    public override void ItemEffect()
    {
        GameManager.Instance.LocalPlayer.ItemEquip(this);
    }
}
