﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Chest : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    [SerializeField] private Sprite lootedSprite;

    [SerializeField] int maxItemCount;
    private List<Item> chestLoot = new List<Item>();

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        StartCoroutine(LootGeneration());
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            StartCoroutine(LootCoroutine());
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            StopAllCoroutines();
        }
    }

    IEnumerator LootGeneration()
    {

        int randomNumOfItem = Random.Range(0, maxItemCount);

        for (int i = 0; i <= randomNumOfItem; i++)
        {
            float randomPerc = Random.Range(0, 100);
            Item currentItem = null;

            foreach(var item in ItemManager.Instance.ItemList)
            {
                if(item.DropRate >= randomPerc)
                {
                    currentItem = item;
                    break;
                }
                else
                {
                    randomPerc -= item.DropRate;
                }
            }


            if (currentItem == null)
                currentItem = ItemManager.Instance.ItemList[0];

            chestLoot.Add(currentItem);
        }

        chestLoot = (from item in chestLoot orderby item.ItemName select item).ToList();
        yield return null;
    }

    IEnumerator LootCoroutine()
    {
        while(true)
        {
            Debug.Log("press F to loot");
            if(Input.GetKeyDown(KeyCode.F))
            {
                Debug.Log("Looted");
                spriteRenderer.sprite = lootedSprite;
                Inventory.Instance.OnLoot(chestLoot);
                Destroy(this);
                break;
            }
            yield return null;
        }

        yield return null;
    }
}
