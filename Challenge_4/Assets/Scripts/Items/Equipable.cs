﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipable : MonoBehaviour
{

    EntityAnimator equipAnimator;
    public EntityAnimator EquipAnimator { get { return equipAnimator; } }

    SpriteRenderer armorSpriteRenderer;
    public SpriteRenderer ArmorSpriteRenderer { get { return armorSpriteRenderer; } }


    private void Awake()
    {
        equipAnimator = GetComponent<EntityAnimator>();
        armorSpriteRenderer = GetComponent<SpriteRenderer>();
    }
}
