﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("custom/items/armor"))]

public class Armor : Item
{
    [SerializeField] private float armorValue;
    public float ArmorValue { get { return armorValue; } }

    [SerializeField] private Equipable armorPrefab;
    public Equipable ArmorPrefab { get { return armorPrefab; } }

    [SerializeField] private Color spriteColor;
    public Color SpriteColor { get { return spriteColor; } }

    public override void ItemEffect()
    {
        GameManager.Instance.LocalPlayer.ItemEquip(this);
    }
}
