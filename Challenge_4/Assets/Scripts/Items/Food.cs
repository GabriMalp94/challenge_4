﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FoodRecoveryType
{
    stamina,
    mana,
}

[CreateAssetMenu(menuName = ("custom/items/food"))]

public class Food : Item
{
    [SerializeField] private FoodRecoveryType foodRecoverytype;
    [SerializeField] private float recoverValue;

    public override void ItemEffect()
    {
        switch(foodRecoverytype)
        {
            case FoodRecoveryType.stamina:
                GameManager.Instance.LocalPlayer.HealthVariateion(recoverValue);
                break;

            case FoodRecoveryType.mana:
                GameManager.Instance.LocalPlayer.CurrentMana += recoverValue;
                break;
        }

        Inventory.Instance.ConsumeItem(this);
    }
}
