﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shootable : MonoBehaviour
{
    [SerializeField] private float speed;
    private float damage;
    SpriteRenderer spriteRenderer;

    public void SetShootWeapon(float _damage, Sprite _sprite, Color _color)
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = _sprite;
        spriteRenderer.color = _color;
        damage = _damage;
    }

    private void Update()
    {
        transform.position += transform.up * speed * Time.deltaTime;
        Destroy(this.gameObject, 10);
    }

    private void OnTriggerEnter2D(Collider2D collision)   
    {
        if(collision.gameObject.TryGetComponent<BulletHit>(out BulletHit currentHit))
        {
            if(collision.TryGetComponent<Stats>(out Stats enemy))
            {
                enemy.HealthVariateion(-damage);
            }
            Destroy(this.gameObject);
        }
    }
}
